package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"google.golang.org/protobuf/proto"
)

var conn net.Conn
var args []string

func main() {
	var wg sync.WaitGroup
	var err error

	conn, err = net.Dial("tcp", "localhost:3000")
	if err != nil {
		panic(err.Error())
	}

	fmt.Println("start connection")
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer conn.Close()
		for {
			var data = make([]byte, MAX_DATA_LENGTH_READ)
			n, err := conn.Read(data)
			if err != nil {
				fmt.Println("Read error")
				return
			}
			data = data[4:]

			fmt.Println(string(data), n)
			fmt.Println()
		}
	}()

	go func() {
		for {
			scanner := bufio.NewScanner(os.Stdin)
			time.Sleep(1 * time.Second)
			fmt.Printf("> ")
			scanner.Scan()
			input := scanner.Bytes()

			args = strings.Split(string(input), " ")
			if len(args) < 1 {
				fmt.Println("missing cmd")
				continue
			}
			cmd, _ := strconv.Atoi(args[0])
			switch uint16(cmd) {
			case CMD_AUTHEN:
				CheckAuthen()
			case CMD_LOGOUT:
				CheckLogout()

			case CMD_CREATE_ROOM:
				CheckCreateRoom()
			case CMD_JOIN_ROOM:
				CheckJoinRoom()
			case CMD_QUICK_JOIN_ROOM:
				CheckQuickJoinRoom()
			case CMD_GET_ROOM_LIST:
				GetRoomList()
			case CMD_LEFT_ROOM:
				CheckLeftRoom()
			case CMD_KICK_FROM_ROOM:
				handleKickFromRoom()
			case CMD_CHAT_IN_ROOM:
				CheckChatInRoom()
			case CMD_READY_GAME:
				CheckReadyGame()
			case CMD_START_GAME:
				CheckStartGame()
			case CMD_SURRENDER_GAME:
				CheckSurrenderGame()
			case CMD_WIN_GAME:
				CheckWinGame()

			case CMD_MOVE_GAME:
				CheckMoveGame()
			case CMD_UNDO_MOVE_REQUEST:
				CheckUndoMoveRequest()
			case CMD_UNDO_MOVE_RESPONSE:
				CheckUndoMoveRespose()
			case CMD_CHANGE_SIDE_GAME:
				CheckChangeSideGame()
			default:
				fmt.Println("Unknown cmd")
			}
		}
	}()

	wg.Wait()
}

// --------------------------------------------------------------------

func CheckAuthen() {
	users := []string{"huy4", "huy5"}
	var username string

	if len(args) == 1 || args[1] == "" {
		username = users[0]
	} else {
		i, err := strconv.Atoi(args[1])
		if err != nil {
			username = users[0]
		} else {
			username = users[i]
		}
	}

	// get jwt token
	postBody, _ := json.Marshal(map[string]string{
		"username": username,
		"password": "123",
	})

	responseBody := bytes.NewBuffer(postBody)
	resp, err := http.Post("http://localhost:8000/login", "application/json", responseBody)
	if err != nil {
		fmt.Println(err.Error())
		panic("checkAuthen: post error")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		panic("checkAuthen: read error")
	}

	var loginRes LoginRes
	json.Unmarshal(body, &loginRes)
	token := loginRes.Token

	// CMD byte
	data := make([]byte, 2)
	binary.LittleEndian.PutUint16(data, (CMD_AUTHEN))

	// data len byte
	dataLenByt := make([]byte, 2)
	binary.LittleEndian.PutUint16(dataLenByt, uint16(len(token)))

	// finaldata
	data = append(data, dataLenByt...)
	data = append(data, []byte(token)...)

	// fmt.Println( data, len(data))

	conn.Write(data)
}

func CheckLogout() {
	SendMessage(CMD_LOGOUT, []byte{})
}

func CheckCreateRoom() {
	if len(args) < 3 {
		fmt.Println("missing args: cmd name pass")
		return
	}
	createRoomRequest := tcp_msg_proto.CreateRoomRequest{Name: args[1], Password: args[2]}
	data, _ := proto.Marshal(&createRoomRequest)

	SendMessage(CMD_CREATE_ROOM, data)
}

func CheckJoinRoom() {
	if len(args) < 2 {
		fmt.Println("missing args: cmd id")
		return
	}

	id, err := strconv.Atoi(args[1])
	if err != nil {
		fmt.Println("wrong args: cmd id (id number)")
		return
	}

	var pass string
	if len(args) < 3 {
		pass = ""
	} else {
		pass = args[2]
	}

	joinRoomRequest := tcp_msg_proto.JoinRoomRequest{Id: int32(id), Password: pass}
	reqData, _ := proto.Marshal(&joinRoomRequest)
	fmt.Println("reqData: ", reqData)

	SendMessage(CMD_JOIN_ROOM, reqData)
}

func CheckQuickJoinRoom() {
	SendMessage(CMD_QUICK_JOIN_ROOM, []byte{})
}

func GetRoomList() {
	// roomList:=  []tcp_msg_proto.RoomInfo{}

	SendMessage(CMD_GET_ROOM_LIST, []byte(""))
}

func CheckLeftRoom() {
	SendMessage(CMD_LEFT_ROOM, []byte(""))
}

func CheckChatInRoom() {
	if len(args) < 2 {
		fmt.Println("missing mess: cmd mess")
		return
	}

	SendMessage(CMD_CHAT_IN_ROOM, []byte(args[1]))
}

func CheckReadyGame() {
	SendMessage(CMD_READY_GAME, []byte(""))
}

func CheckStartGame() {
	SendMessage(CMD_START_GAME, []byte(""))
}

func CheckSurrenderGame() {
	SendMessage(CMD_SURRENDER_GAME, []byte{})
}

func CheckWinGame() {
	SendMessage(CMD_WIN_GAME, []byte{})
}

func CheckMoveGame() {
	if len(args) < 2 {
		fmt.Println("missing move: cmd move")
		return
	}

	SendMessage(CMD_MOVE_GAME, []byte(args[1]))
}

func CheckUndoMoveRequest() {
	SendMessage(CMD_UNDO_MOVE_REQUEST, []byte{})
}

func CheckUndoMoveRespose() {
	if len(args) < 2 {
		SendMessage(CMD_UNDO_MOVE_RESPONSE, []byte{1})
		return
	}

	id, err := strconv.Atoi(args[1])
	if err != nil || id > 1 {
		fmt.Println("wrong args: cmd id (id 0|1)")
		return
	}

	resData := []byte{1}
	if id == 0 {
		resData = []byte{0}
	}
	SendMessage(CMD_UNDO_MOVE_RESPONSE, resData)
}

func CheckChangeSideGame() {
	SendMessage(CMD_CHANGE_SIDE_GAME, []byte{})
}

func handleKickFromRoom() {
	SendMessage(CMD_KICK_FROM_ROOM, []byte{})
}

// --------------------------------------------------------------------

func SendMessage(cmd uint16, bytesData []byte) error {

	length := len(bytesData)
	if length+4 > 65535 {
		return errors.New("exceed data length")
	}

	// CMD byte
	data := make([]byte, 2)
	binary.LittleEndian.PutUint16(data, (cmd))

	// data len byte
	dataLenByt := make([]byte, 2)
	binary.LittleEndian.PutUint16(dataLenByt, uint16(len(bytesData)))

	// finaldata
	data = append(data, dataLenByt...)
	data = append(data, []byte(bytesData)...)

	conn.Write(data)
	return nil
}

func CreateValueWithFixedLength(data []byte, maxLength uint) (val []byte, e error) {
	if len(data) > int(maxLength) {
		e = errors.New("exceed max length")
		return
	}

	val = make([]byte, 0, maxLength)
	val = append(val, data...)
	val = val[:maxLength]

	return
}
