package logger

import (
	"encoding/json"
	"fmt"
	"mooncity-socket-server/config"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger *zap.Logger

func getZapConfig() *zap.Config {
	cfg := config.GetConfig()
	var zapCfg zap.Config
	if cfg.Production {
		zapCfg = zap.NewProductionConfig()
	} else {
		zapCfg = zap.Config{
			OutputPaths:      []string{"stdout"},
			ErrorOutputPaths: []string{"stdout"},
			EncoderConfig: zapcore.EncoderConfig{
				MessageKey: "message",

				LevelKey:    "level",
				EncodeLevel: zapcore.CapitalLevelEncoder,

				TimeKey: "time",

				CallerKey:    "caller",
				EncodeCaller: zapcore.ShortCallerEncoder,
			},
		}
	}
	zapCfg.Level = zap.NewAtomicLevelAt(zapcore.Level(cfg.LoggerLevel))
	zapCfg.Encoding = cfg.LoggerEncoding
	zapCfg.EncoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.UTC().Format("2006-01-02T15:04:05.000Z"))
	}

	return &zapCfg
}

var Info func(string, ...zap.Field)
var Error func(string, ...zap.Field)
var Warn func(string, ...zap.Field)
var Debug func(string, ...zap.Field)

var Object func(interface{})

func init() {
	zapConfig := getZapConfig()
	logger, _ = zapConfig.Build()

	Info = logger.Info
	Error = logger.Error
	Warn = logger.Warn
	Debug = logger.Debug

	Object = printObject
}

func printObject(ob interface{}) {
	x, _ := json.MarshalIndent(ob, "", "")
	fmt.Println(string(x))
}
