package main

type LoginRes struct {
	User struct {
		ID       int    `json:"id"`
		Username string `json:"username"`
	} `json:"user"`
	Token string `json:"token"`
}
