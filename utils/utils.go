package utils

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"encoding/json"
	"errors"
	"mooncity-socket-server/config"
	"mooncity-socket-server/logger"
	"time"

	tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"
	"net"

	"github.com/dgrijalva/jwt-go"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

var cfg *config.Config

type JwtAuthData struct {
	jwt.StandardClaims
	UserID int64 `json:"userId"`
	Type   int64 `json:"type"`
}

func init() {
	cfg = config.GetConfig()
}

func ParseJWT(tokenString string) (*JwtAuthData, error) {
	jwtParse := jwt.Parser{SkipClaimsValidation: false}
	var claims JwtAuthData
	token, err := jwtParse.ParseWithClaims(
		tokenString,
		&claims,
		func(token *jwt.Token) (interface{}, error) {
			return []byte(cfg.JwtSecret), nil
		})
	if err != nil {
		logger.Error("error", zap.Error(err))
		return nil, err
	}
	if !token.Valid {
		logger.Error("token is invalid")
		return nil, errors.New("token is invalid")
	}
	return &claims, err
}

func createJwtToken(claims jwt.Claims) (string, error) {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Sign and get the complete encoded token as a string using the secret
	return token.SignedString([]byte(cfg.JwtSecret))
}

func GetnerateJWTKey(userId int64) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	var (
		claims         JwtAuthData
		standardClaims jwt.StandardClaims = jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 12).Unix()}
	)

	claims = JwtAuthData{UserID: userId, Type: 1, StandardClaims: standardClaims}
	token.Claims = claims

	tokenString, e := token.SignedString([]byte(cfg.JwtSecret))
	if e != nil {
		logger.Error(e.Error())
		return "", nil
	}
	return tokenString, nil
}

func GetBytes(key interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(key)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func ReadTCPMessageBody(dataStream []byte) (res []byte, err error) {
	msgLength := binary.LittleEndian.Uint16(dataStream[2:4])
	if (int(msgLength) + 4) > len(dataStream) {
		return nil, errors.New("received packet error")
	}
	packetBody := dataStream[4 : int64(msgLength)+4]
	return packetBody, nil
}

func CreateResTCPMsg(msgType uint16, msgData []byte) []byte {
	headerTypeData := make([]byte, 2)
	binary.BigEndian.PutUint16(headerTypeData, uint16(msgType))

	responseMsg := tcp_msg_proto.SocketMessage{Data: msgData}
	data, err := proto.Marshal(&responseMsg)
	if err != nil {
		return []byte(err.Error())
	}
	headerLengthData := make([]byte, 2)
	binary.BigEndian.PutUint16(headerLengthData, uint16(len(data)))

	totalBufSize := len(headerTypeData) + len(headerLengthData) + len(data)
	finalRes := make([]byte, totalBufSize)
	finalRes = append(headerTypeData, headerLengthData...)
	finalRes = append(finalRes, data...)
	return finalRes
}

func UserInSlice(s []int64, e int64) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func MinValueInslice(values []float32) (min float32, e error) {
	if len(values) == 0 {
		return 0, errors.New("cannot detect a minimum value in an empty slice")
	}

	min = values[0]
	for _, v := range values {
		if v < min {
			min = v
		}
	}

	return min, nil
}

func RemoveElementByIdx(s []int64, i int) []int64 {
	if i >= len(s) {
		return s
	}
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func CreateIntBytesArr(value int) []byte {
	valueBytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(valueBytes, uint64(value))
	return valueBytes
}

func SendTcpMessage(userConns map[int64]net.Conn, bytesData []byte) {
	for _, conn := range userConns {
		_, _ = conn.Write(bytesData)
	}
}

func SendMessage(conn net.Conn, cmd uint16, bytesData []byte) error {

	length := len(bytesData)
	if length+4 > 65535 {
		return errors.New("exceed data length")
	}

	// CMD byte
	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, (cmd))

	// data len byte
	dataLenByt := make([]byte, 2)
	binary.BigEndian.PutUint16(dataLenByt, uint16(len(bytesData)))

	// finaldata
	data = append(data, dataLenByt...)
	data = append(data, []byte(bytesData)...)

	conn.Write(data)
	return nil
}

func CreateValueWithFixedLength(data []byte, maxLength uint) (val []byte, e error) {
	if len(data) > int(maxLength) {
		e = errors.New("exceed max length")
		return
	}

	val = make([]byte, 0, maxLength)
	val = append(val, data...)
	val = val[:maxLength]

	return
}

func ResponseWrongStruct(conn net.Conn, cmd uint16) {
	data := getActionResponse(false, "Wrong data type")
	SendMessage(conn, cmd, data)
}

func getActionResponse(status bool, msg string) []byte {
	actionResponse := tcp_msg_proto.ActionResponse{Status: status, Msg: msg}
	msgBytes, _ := json.Marshal(actionResponse)
	return msgBytes
}
